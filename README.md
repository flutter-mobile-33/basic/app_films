# app_films

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Credits: [Go](https://www.udemy.com/course/flutter-ios-android-fernando-herrera/)
## Preview

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/12e9e0d3-9d0a-4bdc-9473-804b32c9593e.jpg" alt="Preview" />

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/c209caf6-3b35-4570-9f2a-28f38ba161f3.jpg" alt="Preview" />

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/df4992b5-dba1-4353-9103-2e3c1bc317f1.jpg" alt="Preview" />

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/5e98de84-0c05-4734-ba7f-c2ee223f3316.jpg" alt="Preview" />